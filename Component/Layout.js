import Head from "next/head";
import styles from "../styles/Home.module.css";

const Layout = ({ children }) => {
  return (
    <div className={styles.container}>
      <Head>
        <title>Dewsquad-POC</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>DewSquad</h1>
        {children}
      </main>
    </div>
  );
};

export default Layout;

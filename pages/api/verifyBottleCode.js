/* eslint-disable import/no-anonymous-default-export */
import axios from "axios";
import crypto from "../../helpers/crypto";
export default async (req, res) => {
  let ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
  ip = ip.split(",")[0];
  if (req.method !== "POST") {
    res.status(400).send({ message: "Invalid Request" });
    return;
  }
  const bottleCode = await axios.post(
    `${process.env.serverURL}/checkBottleCode`,
    { data: JSON.stringify(req.body) },
    {
      headers: {
        apiKey: process.env.apiKey,
        ip,
      },
    }
  );
  console.log(bottleCode.data);
  const decryptData = await crypto.decrypt(bottleCode.data.data);
  console.log(decryptData)
  res.status(200).json({ ...JSON.parse(decryptData) });
};

import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { Alert, Button, Form } from "react-bootstrap";
import Link from "next/link";
import axios from "axios";
import { deviceDetect } from "react-device-detect";
import crypto from "../../helpers/crypto";
import getConfig from "next/config";

const CountryPage = (props) => {
  const {code,codeKey} = props 
  const router = useRouter();
  const [error, seterror] = useState("");
  const [success, setSuccess] = useState("");
  const [userId, setUserId] = useState("");
  const [verifyCode, setVerifyCode] = useState("");
  const [token, setToken] = useState("");
  useEffect(() => {
   const decryptToken = JSON.parse(crypto.decrypt(code.data))
   setToken(decryptToken.BottleCode)
  }, []);
  const handleSubmit = async () => {
    try {
      seterror("");
      setSuccess("");
      const browserDetails = deviceDetect();
      const submitRequest = await axios.post("/api/verifyBottleCode", {
        bottleCode: token,
        countryid: 1
      });
      const resData =
        typeof submitRequest === "string"
          ? JSON.parse(submitRequest)
          : submitRequest;
          console.log(resData)
      if (resData.data.iRet == 0) {
          seterror("");
          const submitRequest = await axios.post("/api/redeemKey", {
            bottleCode: token,
            countryid: 1,
            browserDetails,
            roleId: userId,
            codeKey,
            verifyCode
          });
          if(submitRequest.data.iRet === 0){
            setSuccess("Submission success")
          }else{
            seterror("Error in Submission");
          }
        } else {
          seterror("Invalid Bottle Code");
        }
      console.log(submitRequest);
    } catch (error) {
      seterror("Something went wrong!!");
    }
   
  };
  return (
    <div>
      {error && <Alert variant="danger">{error}</Alert>}
      {success && <Alert variant="warning">{success}</Alert>}
      <Form>
        <Form.Group className="mb-3" controlId="formToken">
          <Form.Label>BottleCode</Form.Label>
          <Form.Control type="text" value={token} disabled />
        </Form.Group>
        <Form.Group className="mb-3" controlId="userId">
          <Form.Label>PubgId</Form.Label>
          <Form.Control
            type="text"
            value={userId}
            placeholder="Enter PubgId"
            onChange={(e) => setUserId(e.target.value)}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="userId">
           <Form.Control
            type="text"
            value={verifyCode}
            className="mb-2"
            placeholder="Verification Code"
            onChange={(e) => setVerifyCode(e.target.value)}
          />
            <img src={`https://mrms.igamecj.com/commonAct/cdk_center/valCode.php?sServiceType=pubgmobile&codeKey=${codeKey}`}/>
     
           </Form.Group>

    
        <Button variant="primary" id="submit" onClick={handleSubmit}>
          Submit
        </Button>
      </Form>
      {/* <Link href="/">
        <a className="click">Click here to go home page</a>
      </Link> */}
    </div>
  );
};

export const getServerSideProps = async (ctx) => {
  // const date = new Date().getTime();
  // const expireDate = new Date("01/01/2022 00:00:00").getTime();
  // if (date >= expireDate) {
  //   return {
  //     props: {
  //       initData: {
  //         eligible: false,
  //         promotionStatus: false,
  //       },
  //     },
  //   };
  // } else {
  const token = ctx.query.token;
  const { serverRuntimeConfig } = getConfig();
  const serverURL = `${serverRuntimeConfig.dewURL}/checkKey`;
  const publicip =
    ctx.req.headers["x-forwarded-for"] || ctx.req.connection.remoteAddress;
  console.log(token);
  const bottleCodeRes = await axios.post(
    serverURL,
    {
      data: JSON.stringify({
        bottleCode: token,
        countryid: 1,
      }),
    },
    {
      headers: {
        ip: publicip,
        apikey: "ee356d6fd6dbf62a4523e94748db6a2f",
      },
    }
  );
  const getpubgCode = await axios
    .get(`https://mrms.igamecj.com/commonAct/cdk_center/valCode.php`, {
      params: {
        sServiceType: "pubgmobile",
        getCodeKey: 1,
        responsetype: 1,
      },
    })
  return {
    props: {
        codeKey:getpubgCode.data.key,
        code:bottleCodeRes.data
      
    },
  };
  // }
};

export default CountryPage;

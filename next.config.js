module.exports = {
  reactStrictMode: true,
  serverRuntimeConfig:{
    dewURL :process.env.dewURL,
    key:process.env.localApiKey
  }
}
